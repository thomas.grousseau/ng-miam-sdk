import { NgModule } from '@angular/core';
import { ReadableFloatNumberPipe } from '.';
import { EllipsisPipe } from '.';
import { CapitalizeFirstLetterPipe } from './capitalize-first-letter/capitalize-first-letter.pipe';
import { SafePipe } from './safePipe/safe.pipe';

@NgModule({
  imports: [
  ],
  declarations: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe,
    CapitalizeFirstLetterPipe
  ],
  exports: [
    ReadableFloatNumberPipe,
    EllipsisPipe,
    SafePipe,
    CapitalizeFirstLetterPipe
  ]
})
export class UtilsModule {
  constructor() {}
}
