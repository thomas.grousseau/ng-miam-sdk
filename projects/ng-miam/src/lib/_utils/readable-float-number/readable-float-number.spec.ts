import { ReadableFloatNumberPipe } from './readable-float-number.pipe';

describe('Pipe: readable-float-number', () => {
  let pipe: ReadableFloatNumberPipe;

  beforeEach(() => {
    pipe = new ReadableFloatNumberPipe();
  });

  it('value below 1 should return fractional result', () => {
    expect(pipe.transform('0.5', 'pièces')).toBe('1/2 pièce');
    expect(pipe.transform('0.25', 'pièces')).toBe('1/4 pièce');
    expect(pipe.transform('0.75', 'pièces')).toBe('3/4 pièce');
  });

  it('should be aproximate', () => {
    expect(pipe.transform('0.333333', 'pièces')).toBe('1/3 pièce');
  });

  it('value > 1  should not return fractal result', () => {
    expect(pipe.transform('1.33', 'pièces')).toBe('1.33 pièces');
    expect(pipe.transform('1.75', 'pièces')).toBe('1.75 pièces');
    expect(pipe.transform('1.53')).not.toContain('/');
  });

  it('has a countless unit', () => {
    expect(pipe.transform('1', 'un peu')).toBe('un peu');
    expect(pipe.transform('2', 'un petit peu')).toBe('un petit peu');
    expect(pipe.transform('1', 'un chouilla')).toBe('1 un chouilla');
  });
});