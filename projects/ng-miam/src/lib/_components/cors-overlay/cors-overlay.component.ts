import { Component } from '@angular/core';


@Component({
  selector: 'ng-miam-cors-overlay',
  templateUrl: './cors-overlay.component.html',
  styleUrls: ['./cors-overlay.component.scss']
})
export class CORSOverlayComponent  {

  constructor() { }

  goToMiam(): void {
    document.location.href =  'https://miam.tech/fr/#contact';
  }
}
