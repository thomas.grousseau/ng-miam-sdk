import { ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { Component, OnInit, Input, ViewChild, ElementRef, OnDestroy, Output, EventEmitter, OnChanges } from '@angular/core';
import { stopEventPropagation } from '../../_utils';
import { Subscription } from 'rxjs';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-counter-input',
  templateUrl: './counter-input.component.html',
  styleUrls: ['./counter-input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterInputComponent implements OnDestroy {

  @Input() counter = 1;
  @Input() minRange = 1;
  @Input() maxRange = 100;
  @Input() isXL: boolean;
  @Output() counterChange = new EventEmitter<number>();
  @Output() selected = new EventEmitter<boolean>();

  @ViewChild('counterInput') counterInput: ElementRef;

  private subscriptions: Array<Subscription> = [];
  icon = Icon;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  increment(event: Event): void {
    stopEventPropagation(event);
    this.focus();
    if (this.counter < this.maxRange) {
      this.counter++;
      this.counterChange.emit(this.counter);
      this.cdr.detectChanges();
    }
  }

  decrement(event: Event): void {
    stopEventPropagation(event);
    this.focus();
    if (this.counter > this.minRange) {
      this.counter--;
      this.counterChange.emit(this.counter);
      this.cdr.detectChanges();
    }
  }

  prevent(event: Event): void {
    stopEventPropagation(event);
  }

  updateValue() {
    if (this.counter < this.minRange) {
      this.counter = this.minRange;
    } else if (this.counter > this.maxRange) {
      this.counter = this.maxRange;
    }
    this.counterChange.emit(this.counter);
    this.cdr.detectChanges();
  }

  private focus() {
    this.counterInput.nativeElement.focus();
  }
}
