import { AfterContentChecked, AfterContentInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ContentChildren, 
  QueryList, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { TabItemComponent } from './tab-item.component';

@Component({
  selector: "ng-miam-tabs",
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements AfterContentInit, AfterContentChecked {
  @ContentChildren(TabItemComponent) tabs: QueryList<TabItemComponent>;

  tabItems$: Observable<TabItemComponent[]>;
  activeTabItem: TabItemComponent; // The TabItemComponent currently displayed in the template


  constructor(private cdr: ChangeDetectorRef) {
  }

  ngAfterContentInit(): void {
    this.tabItems$ = this.tabs.changes
      .pipe(
        startWith(this.tabs),     // Here we pipe after tabs.changes has emitted his first change, so we have so set it's current
                                  // value as a starting value and then if there are changes they will be taken into account normally
        map(queryList => queryList.toArray()));
  }

  ngAfterContentChecked() {
    // choose the default tab
    // we need to wait for a next VM turn,
    // because Tab item content, will not be initialized yet
    if (!this.activeTabItem) {
      Promise.resolve().then(() => {
        this.activeTabItem = this.tabs.first;
        this.cdr.detectChanges();
      });
    }
  }

  selectTab(tabItem: TabItemComponent) {
    if (this.activeTabItem === tabItem) {
      return;
    }
    this.activeTabItem = tabItem;
    this.cdr.detectChanges();
  }
}
