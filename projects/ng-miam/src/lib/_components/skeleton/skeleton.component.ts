import { Component, Input } from '@angular/core';
import { Icon} from '../../_types/icon.enum';
import { Skeleton } from '../../_types/skeleton.enum';


@Component({
    selector: 'ng-miam-skeleton',
    templateUrl: './skeleton.component.html',
    styleUrls: ['./skeleton.component.scss']
})
export class SkeletonComponent {

    @Input() type : Skeleton;
    public icon = Icon;
    public skeleton = Skeleton;

    constructor() {
    }

}
