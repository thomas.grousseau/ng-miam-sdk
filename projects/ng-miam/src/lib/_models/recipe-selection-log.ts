import { Resource } from 'ngx-jsonapi';
import * as moment from 'moment';

export class RecipeSelectionLog extends Resource {
  attributes = {
    date: '',
    'user-id': '',
    'recipes-infos': []
  };

  get date(): moment.Moment {
    return moment(this.attributes.date);
  }

  get recipesInfos(): { id, guests }[] {
    return this.attributes['recipes-infos'];
  }
}
