import { Resource } from 'ngx-jsonapi';

export class GroceriesEntry extends Resource {
  public attributes = {
    name: '',
    'capacity-volume': '',
    'capacity-unit': '',
    'capacity-factor': '',
    status: 'active',
    'recipe-ids': []
  };

  get status(): string {
    return this.attributes.status;
  }

  set status(st: string) {
    this.attributes.status = st;
  }
}
