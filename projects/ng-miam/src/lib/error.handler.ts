import {ErrorHandler, Injectable} from "@angular/core";

@Injectable()
export class MiamErrorHandler implements ErrorHandler {
  constructor() {
  }

  handleError(error) {

    console.group('MIAM');
    console.error(error);
    console.groupEnd();

    throw error;
  }

}
