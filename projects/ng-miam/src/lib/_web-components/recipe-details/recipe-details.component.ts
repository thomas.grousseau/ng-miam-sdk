import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, ChangeDetectorRef, Output, EventEmitter, OnChanges, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Recipe } from '../../_models/recipe';
import { Icon } from '../../_types/icon.enum'
import { ContextService, GroceriesListsService, PointOfSalesService, RecipeEventsService } from '../../_services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ng-miam-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecipeDetailsComponent implements OnChanges {
  @Input() recipe: Recipe;
  @Output() recipeAdded: EventEmitter<void> = new EventEmitter();
  @Output() recipeChanged: EventEmitter<void> = new EventEmitter();
  @Output() recipeError: EventEmitter<void> = new EventEmitter();

  public tab = 0;
  public showDetail = true;
  public steps: any[] = [];
  public activeStep = 0;
  public icon = Icon;
  public ingredientsChecked = {};
  protected subscriptions: Subscription[];

  constructor(
    public cdr: ChangeDetectorRef,
    public groceriesListsService: GroceriesListsService,
    protected posService: PointOfSalesService,
    private recipeEventsService: RecipeEventsService,
    private contextService: ContextService) {
    this.subscriptions = [];
  }

  ngOnChanges(): void {
    if (this.recipe) {
      this.steps = this.recipe.relationships['recipe-steps'].data;
      this.cdr.detectChanges();
    }
  }

  changeActiveStep(index: number) {
    this.activeStep = index;
    this.cdr.detectChanges();
  }

  addRecipe(): void {
    this.posService.isPosValid().subscribe(isV => {
      if (isV) {
        if (!this.groceriesListsService.list$.value.hasRecipe(this.recipe.id)){
          this.recipeEventsService.sendEvent(this.recipe, this.recipeEventsService.ACTION_ADDED)
        }
        this.groceriesListsService.appendRecipeToList(this.recipe.id, this.recipe.modifiedGuests);
        this.recipeAdded.emit();
      } else if (this.contextService.invalidPosCallback) {
        localStorage.setItem('_miam/cached-recipe', JSON.stringify(this.recipe));
        this.contextService.invalidPosCallback();
        this.recipeError.emit();
      }
      this.cdr.detectChanges();
    });
  }

  updateGuests(guests) {
    this.recipe.modifiedGuests = guests;
    this.recipeChanged.emit();
    this.cdr.detectChanges();
  }

  print() {
    window.print();
  }
  
  toggleAddon(){
    this.showDetail =  !this.showDetail;
    this.cdr.detectChanges();
  }
}
