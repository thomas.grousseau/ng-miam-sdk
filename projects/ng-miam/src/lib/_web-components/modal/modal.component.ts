import { Component, Input, OnChanges, Output, EventEmitter, ContentChild, TemplateRef, ChangeDetectionStrategy, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { Icon } from '../../_types/icon.enum';

@Component({
  selector: 'ng-miam-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnChanges {
  @Input() title: string;
  @Input() expanded: boolean;
  @Input() confirmButtonText: string;
  @Input() cancelButtonText: string;
  @Input() isAngularComponent: boolean;
  @Input() noHeaderMode: boolean;
  @Output() expandedChange = new EventEmitter<boolean>();
  @Output() close = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() confirm = new EventEmitter();
  
  icon = Icon;

  constructor(
    private cdr: ChangeDetectorRef
  ) { }

  ngOnChanges() {
    if (this.expanded) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }

  onClose() {
    this.hide();
    this.close.emit();
  }

  onCancel() {
    this.hide();
    this.cancel.emit();
  }

  onConfirm() {
    this.hide();
    this.confirm.emit();
  }

  private hide() {
    this.expandedChange.emit(false);
    this.cdr.detectChanges();
  }
}
