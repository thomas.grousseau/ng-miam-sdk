import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DocumentCollection } from 'ngx-jsonapi';
import { Subscription } from 'rxjs';
import { skipWhile } from 'rxjs/operators';
import { Recipe } from '../../_models';
import { RecipesService } from '../../_services';
import { Icon } from '../../_types/icon.enum';
import * as _ from 'lodash';
import { AnalyticsService } from '../../_services/analytics.service';


@Component({
  selector: 'ng-miam-catalog-header',
  templateUrl: './catalog-header.component.html',
  styleUrls: ['./catalog-header.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CatalogHeaderComponent implements OnDestroy, OnInit {
  title = '';
  searchString = '';
  isInCategoryMode = false;
  @Output() returnToCategories: EventEmitter<void>;
  @Output() searchStringChanged: EventEmitter<string>;

  subscriptions: Subscription[];
  recipe: Recipe;
  isLoading: boolean;
  icon = Icon;

  constructor(
    private recipesService: RecipesService,
    private cdr: ChangeDetectorRef) {
    this.returnToCategories = new EventEmitter();
    this.searchStringChanged = new EventEmitter();
    this.subscriptions = [];
    this.isLoading = true;
  }
  ngOnInit(): void {
    this.subscriptions.push(this.recipesService.getRandom({number: 1, size: 1}, { suggested: true })
      .subscribe((result: Recipe[]) => {
        this.recipe = result[0];
        this.isLoading = false;
        this.cdr.detectChanges();
      })
    );
  }

  changeTitle(newTitle: string) {
    this.title = newTitle;
    this.cdr.detectChanges();
  }

  openRecipe() {
    this.recipesService.displayObject(this.recipe, this.recipe.modifiedGuests);
  }

  resetSearchString() {
    this.searchString = '';
    this.cdr.detectChanges();
  }

  searchBar() {
    this.searchStringChanged.emit(this.searchString);
  }

  switchToCategoriesMode() {
    this.returnToCategories.emit();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}
