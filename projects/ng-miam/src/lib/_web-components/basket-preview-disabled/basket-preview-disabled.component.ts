import { Component, OnInit, Input, Output, EventEmitter, HostListener, ChangeDetectorRef } from '@angular/core';
import { BasketEntry } from '../../_models/basket-entry';
import { Icon } from '../../_types/icon.enum';

const MODES_CONFIG = {
  unavailable: { cssClass: 'unavailable', title: 'Article(s) indisponible(s)' },
  removed: { cssClass: 'removed', title: 'Article(s) retiré(s) du panier' },
  oftenDeleted: { cssClass: 'oftenDeleted', title: 'Déjà dans vos placards ?' }
};
const MOBILE_BREAKPOINT = 768;

@Component({
  selector: 'ng-miam-basket-preview-disabled',
  templateUrl: './basket-preview-disabled.component.html',
  styleUrls: ['./basket-preview-disabled.component.scss']
})
export class BasketPreviewDisabledComponent implements OnInit {
  @Input() entries: BasketEntry[];
  @Input() mode = 'unavailable';
  @Input() reduced = false;
  @Output() entryAdded = new EventEmitter<BasketEntry>();
  mobile = window.innerWidth <= MOBILE_BREAKPOINT;
  title: string;
  cssClass: string;
  icon = Icon;

  constructor(
    public cdr: ChangeDetectorRef,
  ) { }

  ngOnInit() {
    if (MODES_CONFIG[this.mode]) {
      this.title = MODES_CONFIG[this.mode].title;
      this.cssClass = MODES_CONFIG[this.mode].cssClass;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.mobile = window.innerWidth <= MOBILE_BREAKPOINT;
    this.cdr.detectChanges();
  }

  toggleReduced() {
    this.reduced = !this.reduced;
    this.cdr.detectChanges();
  }

}
