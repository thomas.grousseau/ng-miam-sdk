import { Component, ViewEncapsulation, ChangeDetectorRef, ChangeDetectionStrategy, Inject } from '@angular/core';
import { RecipesService, GroceriesListsService, ContextService, PointOfSalesService, RecipeEventsService } from '../../_services';
import { AnalyticsService } from '../../_services/analytics.service';
import { DisplayRecipeCardComponent } from '../display-recipe-card/display-recipe-card.component';

/**
 * This is an angular component design to be a web component
 * that's why we use onPushStrategie with the changeDetetectorRef
 * to avoid common issue
 * guide -> https://netbasal.com/a-comprehensive-guide-to-angular-onpush-change-detection-strategy-5bac493074a4
 * and alsow we use ShadowDom to protect style of our component
 * and alsow style of the parent aplication that will use it
 * doc -> https://angular.io/api/core/ViewEncapsulation
 */
@Component({
  selector: 'ng-miam-catalog-recipe-card',
  templateUrl: './catalog-recipe-card.component.html',
  styleUrls: ['./catalog-recipe-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CatalogRecipeCardComponent extends DisplayRecipeCardComponent {

  constructor(
    @Inject(ChangeDetectorRef) cdr: ChangeDetectorRef,
    @Inject(RecipesService) recipeService: RecipesService,
    @Inject(RecipeEventsService) recipeEventsService: RecipeEventsService,
    @Inject(GroceriesListsService) groceriesListsService: GroceriesListsService,
    @Inject(PointOfSalesService) pointOfSalesService: PointOfSalesService,
    @Inject(ContextService) contextService: ContextService,
    @Inject(AnalyticsService) analyticsService: AnalyticsService) {
    super(cdr, recipeService, recipeEventsService, groceriesListsService, pointOfSalesService, contextService, analyticsService);
  }

  openMoreActions() {
    // TODO: Display a menu for the "more actions" button
  }
}
