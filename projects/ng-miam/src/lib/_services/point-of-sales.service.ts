import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { PointOfSale } from '../_models/point-of-sale';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, skipWhile, switchMap, tap } from 'rxjs/operators';
import { SuppliersService } from './suppliers.service';

@Injectable({
  providedIn: 'root'
})
export class PointOfSalesService extends Service<PointOfSale> {
  resource = PointOfSale;
  type = 'point-of-sales';
  pos$ = new BehaviorSubject<PointOfSale>(null);

  constructor(
    private suppliersService: SuppliersService
  ) {
    super();
    this.register();
  }

  loadPos(posId: string): Observable<PointOfSale> {
    if (!this.pos$.value || this.pos$.value.id !== posId) {
      this.get(posId, { include: ['supplier'] }).pipe(
        skipWhile(pos => pos.is_loading),
        tap(pos => this.pos$.next(pos))
      ).subscribe();
    }

    return this.pos$;
  }

  isPosValid(): Observable<boolean> {
    return this.pos$.pipe(
      map(pos => !!pos?.relationships?.supplier?.data?.id)
    );
  }

  loadPosWithExtId(ext_id: string, supplier_id: number) {
    this.all({
      remotefilter:
      {
        'ext-id': ext_id,
        'supplier-id': supplier_id
      },
       include: ['supplier']
    }).pipe(
      skipWhile(pos => pos.is_loading),
      tap(pos => {
        // TODO when data empty => hide webc
        if (pos.data.length > 0) {
          this.pos$.next(pos.data[0]);
        }
      })
    ).subscribe();
    return this.pos$;
  }
}
