import { Injectable } from '@angular/core';
import { Service } from 'ngx-jsonapi';
import { RecipeStatus } from "../_models/recipe-status";
import { BehaviorSubject, Observable, of } from "rxjs";
import { skipWhile, switchMap, tap } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RecipeStatusService extends Service<RecipeStatus> {

  private status: RecipeStatus;
  resource = RecipeStatus;
  type = 'recipe-statuses';

  active$ = new BehaviorSubject<RecipeStatus>(null);
  submitted$ = new BehaviorSubject<RecipeStatus>(null);

  constructor() {
    super();
    this.register();
    this.all().pipe(
      skipWhile(result => result.is_loading)
    ).subscribe(result => {
      this.active$.next(result.data.find(el => el.name === 'ACTIVE'));
      this.submitted$.next(result.data.find(el => el.name === 'SUBMITTED'));
    });
  }

  get active(): Observable<RecipeStatus> {
    return this.active$.asObservable().pipe(skipWhile(res => res == null));
  }

  get submitted(): Observable<RecipeStatus> {
    return this.submitted$.asObservable().pipe(skipWhile(res => res == null));
  }

  /** Only if you are sure value has been loaded */
  get submittedValue(): RecipeStatus {
    return this.submitted$.getValue();
  }

  getCached() {
    return this.status;
  }

  loadCachedStatus(id: string) {
    return of(id).pipe(
      switchMap(_ => this.get(id)),
      tap(status => this.status = status)
    );
  }
}
