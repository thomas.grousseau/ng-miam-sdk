import { Injectable } from '@angular/core';

// Loading a script directly in the DOM every time an event is triggered is probably not the ideal way
// of handling it. However, it seems to be the only way to make sure the analytics script gets loaded,
// the "ga" function defined (and ultimately, called), in the actual context of the host DOM where the webc
// library is going to be injected.
//
// For instance, if the lib is loaded in a third party applications DOM from a Chrome extensions content script,
// attempts at executing the ga function directly in the content script will fail as we cannot access the
// Javascript context where the script got loaded in the first place ("window" object will differ)
@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {
  private _initialized = false;

  constructor() {}

  // Load Google analytics script into host page DOM & create tracker
  public init(key: string) {
    if (!key || key.length < 0) {
      console.error('[Miam] Cannot initialize analytics : invalid key', key);
      return;
    }

    const scriptContent = `
      (function(i, s, o, g, r, a, m) {
        i[r] = i[r] || function() {
          (i[r].q = i[r].q || []).push(arguments);
        }, i[r].l = 1 * new Date().getTime();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m);
      })(window, document, 'script', 'https://ssl.google-analytics.com/analytics.js', 'ga');
      ga('create', '${key}', 'none', 'm_i_a_m_Tracker');
      ga('m_i_a_m_Tracker.set', 'referrer', window.location.origin);
      ga('m_i_a_m_Tracker.set', 'campaignName', 'ng-miam');
      ga('m_i_a_m_Tracker.set', 'campaignSource', 'ng-miam');
      ga('m_i_a_m_Tracker.set', 'campaignMedium', window.location.host);
      ga('m_i_a_m_Tracker.set', 'forceSSL', true);
    `;

    const script = document.createElement('script');
    script.innerHTML = scriptContent;
    document.head.appendChild(script);
    this._initialized = true;
    console.log('[Miam] Initialize analytics');
    this.sendEvent('load');
  }

  // Append new script to the DOM so the event is sent from the correct context
  public sendEvent(action: string, label: string = null, value: number = null) {
    if (this._initialized) {
      const script = document.createElement('script');
      script.innerHTML = `\
        ga('m_i_a_m_Tracker.send', 'event', {
          eventCategory: 'ng-miam',
          eventAction: '${action}',
          eventLabel: '${label}',
          eventValue: ${value?.toFixed()}
        });
      `;
      document.head.appendChild(script);
    }
  }
}
