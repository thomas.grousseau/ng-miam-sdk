
import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { NgMiamElementsModule } from '../src/lib/ng-miam-elements.module';

enableProdMode();

platformBrowserDynamic()
    .bootstrapModule(NgMiamElementsModule,{ ngZone: 'noop' })
